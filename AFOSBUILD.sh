rm -rf /opt/ANDRAX/sippts

WORKDIR=$(pwd)

cd /opt/ANDRAX/bin

rm -rf arpspoof
rm -rf rtcpbleed
rm -rf rtpbleed
rm -rf rtpbleedflood
rm -rf rtpbleedinject
rm -rf sipcrack
rm -rf sipdigestleak
rm -rf sipdump
rm -rf sipenumerate
rm -rf sipexten
rm -rf sipflood
rm -rf sipfuzzer
rm -rf sipinvite
rm -rf sipping
rm -rf siprcrack
rm -rf sipscan
rm -rf sipsend
rm -rf sipsniff
rm -rf siptshark
rm -rf wssend

cd /opt/ANDRAX/PYENV/python3/bin/

rm -rf arpspoof
rm -rf rtcpbleed
rm -rf rtpbleed
rm -rf rtpbleedflood
rm -rf rtpbleedinject
rm -rf sipcrack
rm -rf sipdigestleak
rm -rf sipdump
rm -rf sipenumerate
rm -rf sipexten
rm -rf sipflood
rm -rf sipfuzzer
rm -rf sipinvite
rm -rf sipping
rm -rf siprcrack
rm -rf sipscan
rm -rf sipsend
rm -rf sipsniff
rm -rf siptshark
rm -rf wssend

cd $WORKDIR

source /opt/ANDRAX/PYENV/python3/bin/activate

git clone --recurse-submodules https://github.com/tsundokul/pyradamsa.git
cd pyradamsa

patch libradamsa/libradamsa.c realloc.patch

/opt/ANDRAX/PYENV/python3/bin/pip3 install -r requirements.txt

/opt/ANDRAX/PYENV/python3/bin/python3 setup.py build_ext

/opt/ANDRAX/PYENV/python3/bin/python3 setup.py bdist_wheel

/opt/ANDRAX/PYENV/python3/bin/pip3 install dist/pyradamsa*.whl

cd ..

rm -rf pyradamsa

/opt/ANDRAX/PYENV/python3/bin/pip3 install .

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -R $(pwd) /opt/ANDRAX/sippts

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
